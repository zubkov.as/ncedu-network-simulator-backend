package com.edunetcracker.simulator.model.element.switchNE;

import com.edunetcracker.simulator.model.context.NEContext;
import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;
import com.edunetcracker.simulator.model.dataUnit.ethernet.Ethernet;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.model.port.Port;
import com.edunetcracker.simulator.model.port.RouterPort;
import com.edunetcracker.simulator.model.port.SwitchPort;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.*;

@Setter
@Getter
@Entity(name = "switch")
public class Switch extends NetworkElement {
    private static Logger logger = LoggerFactory.getLogger(Switch.class);

    @OneToMany(mappedBy="switchEl",
               cascade=CascadeType.ALL,
               fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<SwitchPort> ports;

    @Transient
    private SwitchTable switchTable;

    Switch() {
        this.typeName = "switch";
        switchTable = new SwitchTable();
    }

    Switch(int id) {
        super(id);
        this.typeName = "switch";
        switchTable = new SwitchTable();
    }

    //ToDo: SetActive port on connection while running.
    //      SetInactive on disconnect while running.
    @Override
    public void start() {
        ports.stream()
                .filter(port -> null != port.getConnection())
                .forEach(port -> port.setActive(true));
        super.start();
    }


    @Override
    public SequenceStatus copyRefs(NetworkElement another) {
        if (!(another instanceof Switch)) {
            SequenceStatus.PARAMETER_TYPE_INCONSISTENCY.logError("Switch");
            return SequenceStatus.PARAMETER_TYPE_INCONSISTENCY;
        }
        super.copyRefs(another);
        Switch anotherSwitch = (Switch)another;
        this.ports = anotherSwitch.ports;
        return SequenceStatus.OK;
    }

    @Override
    public SequenceStatus copyAutogeneratedValues(NetworkElement another) {
        if (!(another instanceof Switch)) {
            SequenceStatus.PARAMETER_TYPE_INCONSISTENCY.logError("Switch");
            return SequenceStatus.PARAMETER_TYPE_INCONSISTENCY;
        }
        super.copyAutogeneratedValues(another);
        return SequenceStatus.OK;
    }

    @Override
    protected void processContexts() {
        logger.trace("Processing contexts");
        Iterator contextsIterator = this.contexts.iterator();

        while (contextsIterator.hasNext()) {
            NEContext context = (NEContext) contextsIterator.next();
            List<IDataUnit> dataUnits = context.performAction();
            if (null != dataUnits) {
                processDataUnits(dataUnits);
            }
            if (!context.isAlive()) {
                contextsIterator.remove();
            }
        }

        switchTable.onTick();
    }

    //ToDo(Wisp): Take this to super class.
    //            Make List of ports an attribute of super class.
    @Override
    protected void processInputTraffic() {
        Object[] nonemptyPortsArr = ports.stream()
                .filter(Port::hasInput)
                .toArray();
        LinkedList nonemptyPorts = new LinkedList(Arrays.asList(nonemptyPortsArr));
        ListIterator<SwitchPort> portIter = nonemptyPorts.listIterator();

        int processedDUnitsNumber = 0;
        while (processedDUnitsNumber < inputProcessingRate) {
            if (!portIter.hasNext()) {
                if (!nonemptyPorts.isEmpty()) {
                    portIter = nonemptyPorts.listIterator();
                } else { break; }
            }
            SwitchPort curPort = portIter.next();

            if (processPortInput(curPort)) { ++processedDUnitsNumber; }
            else {
                portIter.remove();
            }
        }
        evaluateLoading(processedDUnitsNumber);
    }

    private boolean processPortInput(SwitchPort port) {
        DataUnit dataUnit = port.getIn().poll();
        if (null == dataUnit) {
            return false;
        }
        processDataUnit(dataUnit, port);

        return true;
    }

    @Override
    protected void processCommands() {

    }


    private void processDataUnits (List<IDataUnit> dataUnits) {
        for (IDataUnit dataUnit : dataUnits) {
            if (null != dataUnit) {
                processDataUnit(dataUnit);
            }
        }
    }

    private void processDataUnit (IDataUnit dataUnit) {
        processDataUnit(dataUnit, null);
    }

    private void processDataUnit (IDataUnit dataUnit, SwitchPort inputPort) {
        logger.info("Switch ID{}: Processing dataUnit", getIdNE());

        if (null == dataUnit) { throw new NullPointerException("Null dataUnit"); }

        DataUnit.Type dataUnitType = dataUnit.getType();

        if (dataUnitType == IDataUnit.Type.Ethernet) {
            processEthernet((Ethernet) dataUnit, inputPort);
        }
    }

    private void processEthernet (Ethernet frame, SwitchPort inputPort) {
        logger.info("Switch ID{}: Processing Ethernet.", getIdNE());

        if (null == inputPort) {
            throw new NullPointerException("Null input port.");
        }
        if (null == frame) {
            throw new NullPointerException("Null input frame.");
        }
        if (frame.getEncapsulatedType() != DataUnit.Type.None && !frame.getEncapsulated().isPresent()) {
            return;
        }

        switchTable.addEntry(frame.getSourceMAC(), inputPort.getOrder());

        if (inputPort.getMac().equals(frame.getDestinationMAC()) ||
            Ethernet.BROADCAST_MAC.equals(frame.getDestinationMAC())
        ) {
            processDataUnit(frame.getEncapsulated().get(), inputPort);
        }
        if (inputPort.getMac().equals(frame.getDestinationMAC())) {
            return;
        }
        resendEthernet(frame.getEncapsulated().get().copy(), frame.getDestinationMAC(), frame.getSourceMAC(), inputPort);
    }

    private void sendEthernet (IDataUnit duToEncapsulate, Long destinationMac) {
        if (null == duToEncapsulate) {
            throw new NullPointerException("Null duToEncapsulate");
        }
        if (null == destinationMac) {
            throw new NullPointerException("Null destinationMac");
        }

        if (Ethernet.BROADCAST_MAC.equals(destinationMac)) {
            for (SwitchPort port : ports) {
                Ethernet frame = new Ethernet();
                frame.encapsulate(duToEncapsulate.copy());
                frame.setDestinationMAC(destinationMac);
                frame.setSourceMAC(port.getMac());

                port.push(frame);
            }
        }

        Integer portNumber = switchTable.getPortNumber(destinationMac);
        SwitchPort sendingPort = getPort(portNumber);

        Ethernet frame = new Ethernet();
        frame.encapsulate(duToEncapsulate);
        frame.setDestinationMAC(destinationMac);
        frame.setSourceMAC(sendingPort.getMac());

        sendingPort.push(frame);
    }

    private void resendEthernet (IDataUnit duToEncapsulate, Long destinationMac, Long sourceMac, SwitchPort inputPort) {
        if (null == duToEncapsulate) {
            throw new NullPointerException("Null duToEncapsulate");
        }
        if (null == destinationMac) {
            throw new NullPointerException("Null destinationMac");
        }
        if (null == sourceMac) {
            throw new NullPointerException("Null sourceMac");
        }

        if (Ethernet.BROADCAST_MAC.equals(destinationMac)) {
            logger.info("Switch ID {}: Resending broadcast frame.", getIdNE());
            for (SwitchPort port : ports) {
                if (!port.isActive() ||
                    port == inputPort
                ) { continue; }

                Ethernet frame = new Ethernet();
                frame.encapsulate(duToEncapsulate.copy());
                logger.info("Encapsulated {}.", frame.getEncapsulated().get().getType());
                frame.setDestinationMAC(destinationMac);
                frame.setSourceMAC(sourceMac);

                port.push(frame);
            }
        }

        Integer portNumber = switchTable.getPortNumber(destinationMac);
        if (null == portNumber) { return; }
        SwitchPort sendingPort = getPort(portNumber);
        if (null == sendingPort) {
            logger.warn("Tried to find port by not existing portNumber.");
            return;
        }

        Ethernet frame = new Ethernet();
        frame.encapsulate(duToEncapsulate);
        frame.setDestinationMAC(destinationMac);
        frame.setSourceMAC(sourceMac);

        sendingPort.push(frame);
    }

    /**
     * Returns port with number given. (Each port has it's unique number in a NetworkElement.)
     * @param portNumber
     * @return
     */
    public SwitchPort getPort(int portNumber) {
        for (SwitchPort port : ports) {
            if (portNumber == port.getOrder()) {
                return port;
            }
        }
        return null;
    }

    //    @Override
//    void performAction(Object action) {
//
//    }

}
