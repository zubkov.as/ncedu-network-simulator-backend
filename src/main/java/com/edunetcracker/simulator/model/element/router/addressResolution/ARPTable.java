package com.edunetcracker.simulator.model.element.router.addressResolution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ARPTable {
    Logger logger = LoggerFactory.getLogger(ARPTable.class);

    private Map<Integer, Entry> entries;

    public ARPTable () {
        entries = new HashMap<>();
    }

    public Long getMAC (Integer ip) {
        if (null == ip) {

            return null;
        }
        Entry entry = entries.get(ip);
        if (null == entry) {
            return null;
        }
        return entry.mac;
    }

    public void addEntry (Integer ip, Long mac) {
        Entry newEntry = new Entry(ip, mac);
        entries.put(ip, newEntry);
    }

    public void onTick () {
        entries.values().removeIf(nextEntry -> --nextEntry.ticksToLive <= 0);
    }


    public static class Entry {
        final int DEFAULT_TICKS_TO_LIVE = 1024;

        Integer ip;
        Long mac;
        int ticksToLive;

        private Entry (Integer ip, Long mac) {
            this.ip = ip;
            this.mac = mac;
            ticksToLive = DEFAULT_TICKS_TO_LIVE;
        }
    }
}
