package com.edunetcracker.simulator.model;

import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.*;

/**
 * A representation of a file the user is working with.
 */

@Getter
@Setter
@Entity(name = "scene")
public class Scene implements DBObject<Scene> {
    @Transient
    private Logger logger = LoggerFactory.getLogger(Scene.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private long id;

    @Column
    @JsonProperty
    private String name;

//ToDo! ToDo! ToDo-ToDo-ToDo! ToDo-ToDooooooooooooo-ToDo-DoDoDo!
//    @ManyToMany
//    private List<User> users;


    @JsonProperty
    @OneToMany(mappedBy = "scene",
               cascade=CascadeType.ALL,
               fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    private List<NetworkElement> networkElements;


    @JsonProperty
    @OneToMany(mappedBy = "scene",
               cascade=CascadeType.ALL,
               fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    private List<com.edunetcracker.simulator.model.Link> links;


    public Scene () {
        networkElements = new LinkedList<>();
        links = new LinkedList<>();
    }


    @Override
    public SequenceStatus copyRefs(Scene another) {
        networkElements = another.networkElements;
        links = another.links;
        return SequenceStatus.OK;
    }

    @Override
    public SequenceStatus copyAutogeneratedValues(Scene another) {
        id = another.id;
        return SequenceStatus.OK;
    }

    /**
     * Makes Components know that they are connected to each other with a wire, and saves the connection to
     * a running scene configuration.
     * @param id1 first Component
     * @param id2 second Component
     */
    public void link (long id1, long id2) {
    }

    public void addNetworkElement (NetworkElement networkElement) {
        networkElements.add(networkElement);
    }

    public void addLink(Link link) {
        links.add(link);
    }
}
