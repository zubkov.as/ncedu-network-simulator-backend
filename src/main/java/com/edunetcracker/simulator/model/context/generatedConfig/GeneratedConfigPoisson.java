package com.edunetcracker.simulator.model.context.generatedConfig;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeneratedConfigPoisson extends GeneratedConfig {
    private long mean;

    public GeneratedConfigPoisson(long routerId, boolean isAlive, String destinationIp, long mean) {
        super(routerId, isAlive, destinationIp);
        this.mean = mean;
    }
}
