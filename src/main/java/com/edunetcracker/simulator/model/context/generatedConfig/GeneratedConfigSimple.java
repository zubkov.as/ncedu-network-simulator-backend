package com.edunetcracker.simulator.model.context.generatedConfig;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeneratedConfigSimple extends GeneratedConfig {
    //time between each package is sent
    private long timeBetPacks;

    public GeneratedConfigSimple(long routerId, boolean isAlive, String destinationIP, long timeBetPacks) {
        super(routerId, isAlive, destinationIP);
        this.timeBetPacks = timeBetPacks;
    }
}
