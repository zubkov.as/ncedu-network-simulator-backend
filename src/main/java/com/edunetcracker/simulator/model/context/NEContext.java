package com.edunetcracker.simulator.model.context;

import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;

import java.util.List;

public interface NEContext { // todo make abstract :) again

    Long getId();

    /**
     * Checks whether inner context's timer is run out
     */
    boolean isAlive();

    /**
     * Performs action
     * @return A set of DataUnits to be sent somewhere else
     */
    List<IDataUnit> performAction();

    /**
     * Is called on the stage when the input is being processed
     * changes context's inner variables
     * @param dataUnit - a package to analyse
     */
    boolean performInput(DataUnit dataUnit);

    //TODO(Wisp): Comparator for contexts
    boolean equals (NEContext other);
}
