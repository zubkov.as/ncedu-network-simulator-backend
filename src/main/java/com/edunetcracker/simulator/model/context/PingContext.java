package com.edunetcracker.simulator.model.context;

import com.edunetcracker.simulator.model.DTO.SocketMessageDTO;
import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;
import com.edunetcracker.simulator.service.WSService;
import com.edunetcracker.simulator.service.dataUnit.IP.IpBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;



@Setter
@Getter
@NoArgsConstructor
public class PingContext implements NEContext {
    private static Logger logger = LoggerFactory.getLogger(PingContext.class);

    @Deprecated
    private WSService wsService;

    @Deprecated
    private Long sessionId;

    private static final int sendingInterval = 10;

    @JsonProperty
    private Long id;
    private boolean isAlive;

    @JsonProperty
    private Integer sourceIp;
    @JsonProperty
    private Integer destinationIp;

    private boolean gotResponse = false;
    private boolean firstIteration = true;
    private int pingsLeft;
    private int tickNumber;
    private int failureNumber;


    @Override
    public boolean isAlive() {
        return this.isAlive;
    }

    private boolean responsive;



    @Override
    public List<IDataUnit> performAction() {
        List<IDataUnit> retList = new LinkedList<>();
        SocketMessageDTO messageDTO = null;

        if (firstIteration) {
            firstIteration = false;
            retList.add(IpBuilder.ping(id, sourceIp, destinationIp));
        }

        if (++tickNumber >= sendingInterval) {
            logger.info("Ping id{}: no response.", id);
            gotResponse = true;
            failureNumber++;
            messageDTO = new SocketMessageDTO(sessionId.toString(),
                    SocketMessageDTO.PING,
                    id.toString(),
                    ".");

        }
        if (gotResponse || responsive) {
            logger.info("Ping id{}: {} iterations left.", id, pingsLeft);
            gotResponse = false;
            tickNumber = 1;

            for (int i = 0; i < (responsive ? pingsLeft : 1); i++) {
                retList.add(IpBuilder.ping(id, sourceIp, destinationIp));
                pingsLeft--;
            }

            if (null == messageDTO) {
                messageDTO = new SocketMessageDTO(sessionId.toString(),
                                                  SocketMessageDTO.PING,
                                                  id.toString(),
                                                  "!");
            }

            if (pingsLeft <= 0) {
                logger.info("Ping context with id {} is dead.", id);
                isAlive = false;
                if (!responsive)
                    return null;
            }

            if (!responsive)
               wsService.send(messageDTO);
        }

        return retList;
    }

    @Override
    public boolean performInput(DataUnit dataUnit) {
        gotResponse = true;

        if (responsive) {
            isAlive = true;
            pingsLeft++;
        }
//        if (numOfSend == maxSendIters) {
//            state = PingState.COMPLETED;
//        } else {
//            state = PingState.SEND;
//            numOfSend++;
//            if (/*receive reply == */true) {
//                numOfGet++;
//                message.append("!");
//            } else {
//                message.append(".");
//            }
//        }

        return true;
    }

    @Override
    public boolean equals (NEContext another) {
        if (null == another) {
            return false;
        }
        return id == another.getId();
    }
}
