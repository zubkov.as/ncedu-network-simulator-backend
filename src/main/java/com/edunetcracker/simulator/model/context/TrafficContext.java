package com.edunetcracker.simulator.model.context;

import com.edunetcracker.simulator.model.context.generatedConfig.*;
import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;
import com.edunetcracker.simulator.model.dataUnit.ip.IP;
import com.edunetcracker.simulator.service.dataUnit.IP.IpBuilder;
import com.edunetcracker.simulator.service.routingService.IpService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Getter
@Setter
public class TrafficContext implements NEContext {

    private GeneratedConfig trafficConfig;
    //mod of the time left from tact
    private long send;

    private Long id;

    //the interval between which we call the function once
    static final long TIME_TACT = 100; //todo: find a proper value

    //receive object, generator configuration
    //later: add different types of generator (like dataunit)
    public TrafficContext(GeneratedConfig trafficInit) {
        send = 0;
        this.trafficConfig = trafficInit;

        this.id = (long) (new Random().nextInt());
    }


    @Override
    public boolean isAlive() {
        return trafficConfig.isAlive();
    }

    @Override
    public List<IDataUnit> performAction() {
        List<IDataUnit> contextList = new LinkedList<>();

        send += TIME_TACT;

        if (trafficConfig instanceof GeneratedConfigSimple) {
            GeneratedConfigSimple generatedConfigSimple = (GeneratedConfigSimple) trafficConfig;
            for (int i = 0; i < send / generatedConfigSimple.getTimeBetPacks(); i++) {
                //Router router = RouterService.getInstance().getLoaded(trafficConfig.getRouterId());
                //NEContext ping = ContextService.getInstance().ping(router.getSessionId(), null, IpService.intFromString(trafficConfig.getDestinationIP()));
                //router.getContexts().add(ping);


                //DataUnit dataUnit = contextBuilder(DataUnit.Type.IP, null);
                contextList.add(IpBuilder.traffic(id, null,IpService.intFromString(trafficConfig.getDestinationIP())));

                //contextList.add(dataUnit);


            }

            send = send % generatedConfigSimple.getTimeBetPacks();
        }

        else {
            long timeBetPack = timeBetPacks(trafficConfig);
            while (send / timeBetPack > 0) {
                send -= timeBetPack;
                timeBetPack = timeBetPacks(trafficConfig);
                //Router router = RouterService.getInstance().getLoaded(trafficConfig.getRouterId());
                //NEContext ping = ContextService.getInstance().ping(router.getSessionId(), null, IpService.intFromString(trafficConfig.getDestinationIP()));
                //router.getContexts().add(ping);

                //DataUnit dataUnit = contextBuilder(DataUnit.Type.IP, null);
                contextList.add(IpBuilder.traffic(id, null,IpService.intFromString(trafficConfig.getDestinationIP())));

                //contextList.add(dataUnit);
            }

            send = send % timeBetPack;
        }


        return contextList;
    }

    @Override
    public boolean performInput(DataUnit dataUnit) {
        //is not needed for this function
        return false;
    }

    @Override
    public boolean equals(NEContext another) {
        if (null == another) {
            return false;
        }
        return this.getId() == another.getId();
    }

    public static DataUnit contextBuilder(DataUnit.Type type, String extraData) {
        DataUnit newPacket = new IP() {
            @Override
            public void setType(Type type) {
                super.setType(type);
            }

            @Override
            public void setExtraData(String extraData) {
                super.setExtraData(extraData);
            }
        };

        return newPacket;
    }

    private static long timeBetPacks(GeneratedConfig gc){
        double timeBet = 0;
        if(gc instanceof GeneratedConfigUniform) {
            GeneratedConfigUniform gcu = (GeneratedConfigUniform) gc;
            timeBet = new UniformRealDistribution(gcu.getMinTimeBet(), gcu.getMaxTimeBet()).sample();
        }
        if(gc instanceof GeneratedConfigNormal) {
            GeneratedConfigNormal gcn = (GeneratedConfigNormal) gc;
            timeBet = new NormalDistribution(gcn.getMean(),gcn.getStandardDeviation()).sample();
        }
        if(gc instanceof GeneratedConfigPoisson) {
            GeneratedConfigPoisson gcp = (GeneratedConfigPoisson)gc;
            timeBet = new PoissonDistribution(gcp.getMean()).sample();
        }
        return Math.max(1, (long) timeBet); //todo: return error
    }

}
