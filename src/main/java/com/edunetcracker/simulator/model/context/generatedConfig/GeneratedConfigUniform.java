package com.edunetcracker.simulator.model.context.generatedConfig;

import lombok.Getter;
import lombok.Setter;

/**
 * The input parameters for Even distribution of traffic
 */
@Getter
@Setter
public class GeneratedConfigUniform extends GeneratedConfig {
    //minimum time between packages
    private long minTimeBet;
    //maximum time between packages
    private long maxTimeBet;

    public GeneratedConfigUniform(long routerId, boolean isAlive, String destinationIP, long minTimeBet, long maxTimeBet) {
        super(routerId, isAlive, destinationIP);
        this.minTimeBet = minTimeBet;
        this.maxTimeBet = maxTimeBet;
    }
}
