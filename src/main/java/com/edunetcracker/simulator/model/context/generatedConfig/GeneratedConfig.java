package com.edunetcracker.simulator.model.context.generatedConfig;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
/**
 * This class is created in order to keep parameters together
 * and not to send them separately each time
 * (an initialisation for Traffic)
 */
public class GeneratedConfig {
    private long routerId;
    private boolean isAlive;
    private String destinationIP;

    GeneratedConfig(){}
}
