package com.edunetcracker.simulator.model.context.generatedConfig;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeneratedConfigNormal extends GeneratedConfig {
    private double mean;
    private double standardDeviation;

    public GeneratedConfigNormal(long routerId,boolean isAlive, String destinationIP, double mean,
                                 double standardDeviation) {
        super(routerId, isAlive, destinationIP);
        this.mean = mean;
        this.standardDeviation = standardDeviation;
    }
}
