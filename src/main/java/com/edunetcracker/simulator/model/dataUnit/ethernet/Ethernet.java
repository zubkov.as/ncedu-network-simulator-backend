package com.edunetcracker.simulator.model.dataUnit.ethernet;

import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

public class Ethernet extends DataUnit {
    public static final Long BROADCAST_MAC = 0L;

    @Getter
    @Setter
    private Long sourceMAC;

    @Getter
    @Setter
    private Long destinationMAC;

    public Ethernet () {
        super();
        setType(Type.Ethernet);
    }

    @Override
    public IDataUnit copy() {
        Ethernet newEthernet = new Ethernet();
        newEthernet.setSourceMAC(sourceMAC);
        newEthernet.setDestinationMAC(destinationMAC);
        newEthernet.setEncapsulatedType(getEncapsulatedType());
        newEthernet.setExtraData(getExtraData());

        getEncapsulated().ifPresent(dataUnit -> newEthernet.encapsulate(dataUnit.copy()));

        return newEthernet;
    }
}
