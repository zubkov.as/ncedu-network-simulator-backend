package com.edunetcracker.simulator.model.dataUnit.ICMP;

import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;
import com.edunetcracker.simulator.model.dataUnit.ip.IP;
import lombok.Getter;
import lombok.Setter;

public class ICMP extends DataUnit {

    //@Getter
    //@Setter
    private TypeICMP type;


    public ICMP() {
        setType(Type.ICMP);
    }

    @Override
    public IDataUnit copy() {
        ICMP newICMP = new ICMP();
        newICMP.setEncapsulatedType(getEncapsulatedType());
        newICMP.setExtraData(getExtraData());

        getEncapsulated().ifPresent(dataUnit -> newICMP.encapsulate(dataUnit.copy()));

        return newICMP;
    }

    public enum TypeICMP{
        ECHO_REPLY,
        DEST_UNREACH,
        ECHO_REQ,
        TIME_EXCEEDED
    }



}
