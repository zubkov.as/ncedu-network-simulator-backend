package com.edunetcracker.simulator.model.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaticRouteDTO {

    private Long routerId;
    private String ip;
    private String mask;
    private String nextHop;
}
