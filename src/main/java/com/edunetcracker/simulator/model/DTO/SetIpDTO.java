package com.edunetcracker.simulator.model.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SetIpDTO {
    Long routerId;
    Integer portNumber;
    String ip;
    String mask;
}
