package com.edunetcracker.simulator.model.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Getter
@Setter
@Entity(name = "scene")
@NoArgsConstructor
@AllArgsConstructor
public class SceneDTO {

    @Id
    @JsonProperty
    private long id;

    @Column
    @JsonProperty
    private String name;
}
