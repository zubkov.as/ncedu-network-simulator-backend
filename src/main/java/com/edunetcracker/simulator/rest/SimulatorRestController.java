package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.DTO.PingDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.context.NEContext;
import com.edunetcracker.simulator.model.context.generatedConfig.*;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.service.LinkService;
import com.edunetcracker.simulator.service.RouterService;
import com.edunetcracker.simulator.service.SimulatorService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/simulation")
public class SimulatorRestController extends RestControllerImpl {
    private static Logger logger = LoggerFactory.getLogger(SimulatorRestController.class);

    private final RouterService routerService;
    private final LinkService linkService;
    private final SimulatorService simulatorService;

    @Autowired
    public SimulatorRestController(RouterService routerService, SimulatorService simulatorService, LinkService linkService) {
        this.routerService = routerService;
        this.simulatorService = simulatorService;
        this.linkService = linkService;
    }


    /**
     * Creates a new link.
     * @param link JSON with such fields:
     *             "sceneId":"",
     * 	           "portAid":"",
     * 	           "portZid":""
     * @return
     */
    @RequestMapping(value = "/link", method = RequestMethod.POST)
    public ResponseEntity linkComponents(@RequestBody Link link) {
        SequenceStatus ss = linkService.linkComps(link);
        logger.info("ConnA ok: {}", link.getConnA().getPort().getConnection() == link.getConnA());
        logger.info("ConnZ ok: {}", link.getConnZ().getPort().getConnection() == link.getConnZ());
        return ResponseEntity.ok(toJson(link));
    }

    @RequestMapping(value = "/link", method = RequestMethod.DELETE)
    public ResponseEntity deleteLink(@RequestBody Long id) {
        SequenceStatus status = linkService.delete(id);
        return ResponseEntity.status(status.getHttpStatus())
                .body(status.getHttpBody());
    }

    /*Returns saved state of network
     * later we can add args to load one of many saved states*/
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<List<NetworkElement>> getComponents() {
        return simulatorService.getComps();
    }

    /*Starts all components
     * returns OK*/
    @RequestMapping(value = "/startAll", method = RequestMethod.POST)
    public ResponseEntity startAllComponents (@RequestBody Long sceneId) {
        return simulatorService.startAllComponents(sceneId);
    }

    /**
     * Starts only stated components
     * input int[] id with id of those components which you want to start
     */
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ResponseEntity startComponent (@RequestBody Long idNE) {
        return simulatorService.startComponent(idNE);
    }

    /*Stops all components
     * returns OK*/
    @RequestMapping(value = "/stopAll", method = RequestMethod.POST)
    public ResponseEntity stopAllComponents(@RequestBody Long sceneId) {
        return simulatorService.stopAllComponents(sceneId);
    }

    /*Stops only stated components
     * input int[] id with id of those components which you want to stop
     * returns OK*/
    @RequestMapping(value = "/stop", method = RequestMethod.POST)
    public ResponseEntity stopComponent(@RequestBody Long idNE) {
        return simulatorService.stopComponent(idNE);
    }

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity ping(@RequestBody PingDTO pingDTO) {
        System.out.println("ping prishol");

//        try {
//            wsService.sendPing(5, "UHHU");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        NEContext pingContext = simulatorService.ping(pingDTO);
        return ResponseEntity.ok(toJson(pingContext));
    }

    @RequestMapping(value = "/fast_launch", method = RequestMethod.POST)
    public ResponseEntity fastLaunch () {
        Router router1 = new Router();
        router1.setPhysConfigType("any");
        router1.setSessionId(1L);
        routerService.create(router1);

        Router router2 = new Router();
        router2.setPhysConfigType("any");
        router2.setSessionId(1L);
        routerService.create(router2);

        Link link = new Link();
        link.setPortAid(router1.getPort(1).getId());
        link.setPortZid(router2.getPort(1).getId());
        linkService.linkComps(link);

        //simulatorService.startAllComponents();

        routerService.setIpAddress(router1.getIdNE(), 1, "192.168.12.1", "255.255.255.0");
        routerService.setIpAddress(router2.getIdNE(), 1, "192.168.12.2", "255.255.255.0");

        return ResponseEntity.ok(String.format("Launched routers with ids %d and %d successfully.",
                                                router1.getIdNE(), router2.getIdNE()));
    }

    //todo(dooly): write rest
    @RequestMapping(value = "/traffic/simple", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody GeneratedConfigSimple generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

    @RequestMapping(value = "/traffic/normal", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody GeneratedConfigNormal generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

    @RequestMapping(value = "/traffic/uniform", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody GeneratedConfigUniform generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

    @RequestMapping(value = "/traffic/poisson", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody GeneratedConfigPoisson generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

//    @RequestMapping(value = "/test_rest", method = RequestMethod.POST)
//    public ResponseEntity testRest (@RequestBody TrafficDTO trafficDTO) {
//        Test.doTest(trafficDTO);
//        return ResponseEntity.ok("Test section is ok");
//    }

//    /**
//     * Here we try to launch our project with given inputs
//     * Just a simple test, in order to see if everything is working
//     * @return statement that everything is "ok"
//     */
//    @RequestMapping(value = "/fast_launch", method = RequestMethod.POST)
//    public ResponseEntity fastLaunch () {
//        Router router1 = new Router();
//        router1.setPhysConfigType("any");
//        routerService.create(router1);
//
//        Router router2 = new Router();
//        router2.setPhysConfigType("any");
//        routerService.create(router2);
//
//        Link link = new Link();
//        link.setPortAid(router1.getPort(1).getId());
//        link.setPortZid(router2.getPort(1).getId());
//        linkService.linkComps(link);
//
//        simulatorService.startComponents();
//
//        routerService.setAddress(router1.getIdNE(), 1, "192.168.0.1", "255.255.255.0");
//        routerService.setAddress(router2.getIdNE(), 1, "192.168.0.2", "255.255.255.0");
//
//        TrafficDTO trafficDTO = new TrafficDTO();
//        trafficDTO.setDestIp1("192.168.0.2");
//        trafficDTO.setDestIp2("192.168.0.1");
//        trafficDTO.setRouterId1(1L);
//        trafficDTO.setRouterId2(2L);
//
//        Test.doTest(trafficDTO);
//        return ResponseEntity.ok("Test section is ok");
//    }
}
