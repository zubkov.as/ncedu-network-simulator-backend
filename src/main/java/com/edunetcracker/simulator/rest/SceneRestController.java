package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.Scene;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.service.RouterService;
import com.edunetcracker.simulator.service.SceneService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/scene")
public class SceneRestController extends RestControllerImpl {

    private final SceneService sceneService;
    private final RouterService routerService;

    @Autowired
    public SceneRestController(SceneService sceneService, RouterService routerService) {
        this.sceneService = sceneService;
        this.routerService = routerService;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ResponseEntity newScene (@RequestBody String sceneName) {
        Scene newScene = new Scene();
        newScene.setName(sceneName);
        SequenceStatus ss = sceneService.create(newScene);
        return ResponseEntity.ok(new Gson().toJson(newScene.getId()));
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity getSceneById (@RequestParam int id){
        Scene scene = sceneService.get(id);
        return ResponseEntity.ok(toJson(scene));
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity saveScene (@RequestBody Long id) {
        Scene scene = sceneService.update(id);
        return ResponseEntity.ok(toJson(scene));
    }

    @RequestMapping(value = "/change", method = RequestMethod.GET)
    public ResponseEntity update (int id, Scene scene) {
        Scene newScene = sceneService.change(id,scene);
        return ResponseEntity.ok(toJson(newScene));
    }

    @RequestMapping(value = "/get_names", method = RequestMethod.GET)
    public ResponseEntity getSceneNames () {
        return ResponseEntity.ok(sceneService.getSceneNames());
    }

    @RequestMapping(value = "/loading_test", method = RequestMethod.GET)
    public ResponseEntity testLoading (){
        Scene scene = sceneService.get(1);
        Router router11 = (Router)scene.getNetworkElements().get(0);
        Router router12 = routerService.getLoaded(1);

        Router router21 = routerService.getLoaded(2);
        Router router22 = (Router)scene.getNetworkElements().get(1);

        Scene scene1 = scene.getNetworkElements().get(0).getScene();
        Scene scene2 = scene.getNetworkElements().get(1).getScene();
        return (ResponseEntity.ok(scene.getNetworkElements().get(0).getScene() == null));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity removeSceneById (@RequestBody int id){
        SequenceStatus ss = sceneService.delete(id);
        return ResponseEntity.status(ss.getHttpStatus())
                             .body  (ss.getHttpBody());
    }
}
