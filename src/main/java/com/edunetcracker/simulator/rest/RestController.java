package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.DBObject;

public interface RestController {

    /**
     * Creates a JSON out of a Object
     * @param obj instance of Object with @JsonAnnotated fields
     * @return Json
     */
    String toJson (Object obj);
}
