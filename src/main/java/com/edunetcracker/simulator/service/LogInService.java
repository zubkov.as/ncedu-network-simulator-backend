package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.model.DTO.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Service
public class LogInService {




    private static ArrayList<UserDTO> users = new ArrayList<UserDTO>();
    @PostConstruct
    private void onInit(){
        UserDTO user = new UserDTO("login","pass", "111");
        users.add(user);

    }
    public  ResponseEntity logIn(UserDTO user){
        for (UserDTO elem : users) {
            if (user.getLogin().equals(elem.getLogin()) && user.getPass().equals(elem.getPass())) {
                return ResponseEntity.status(HttpStatus.OK).body(elem.getId());
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

    }
}
