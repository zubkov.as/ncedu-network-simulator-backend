package com.edunetcracker.simulator.service;


public abstract class DBService<T> implements CRUDService<T> {

    /**
     * Adds an instance of bean and all it's underling beans to services' track lists.
     * @param instance Bean to 'load'.
     */
    abstract protected void trackRecursively(T instance);

    /**
     * Removes a bean and all it's underling beans from services' track lists.
     * @param instance Bean to 'unload'.
     */
    abstract protected void untrackRecursively(T instance);

    /**
     * Drops the instance's representation from the database.
     * @param instance Object to delete from database.
     */
    abstract protected void drop(T instance);
}
