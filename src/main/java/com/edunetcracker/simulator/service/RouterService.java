package com.edunetcracker.simulator.service;


import com.edunetcracker.simulator.database.repository.networkElementRepository.RouterRepository;
import com.edunetcracker.simulator.model.DTO.SetIpDTO;
import com.edunetcracker.simulator.model.DTO.StaticRouteDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.Scene;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.port.RouterPort;
import com.edunetcracker.simulator.model.element.router.routing.routingTable.RoutingTable;
import com.edunetcracker.simulator.service.configurers.RouterConfigurer;

import com.edunetcracker.simulator.service.context.ContextService;
import com.edunetcracker.simulator.service.routingService.IpService;
import com.edunetcracker.simulator.service.routingService.RoutingTableService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.Synchronized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RouterService extends DBService<Router> {
    private static Logger logger = LoggerFactory.getLogger(RouterService.class);

    private final WSService wsService;
    private final ContextService contextService;
    private final RouterRepository routerRepository;
    private final RouterConfigurer routerConfigurer;
    private final PortService portService;
    private final RoutingTableService routingTableService;
    private final LinkService linkService;
    @Getter
    private static RouterService instance;

    @Getter
    private List<Router> loadedRouters = new ArrayList<>();

    @Autowired
    public RouterService(WSService wsService, ContextService contextService, RouterRepository routerRepository, RouterConfigurer routerConfigurer, PortService portService, RoutingTableService routingTableService, LinkService linkService) {
        this.wsService = wsService;
        this.contextService = contextService;
        this.routerRepository = routerRepository;
        this.routerConfigurer = routerConfigurer;
        this.portService = portService;
        this.routingTableService = routingTableService;
        this.linkService = linkService;
        this.instance = this;
    }

    @Override
    public Router getLoaded(long id) {
        for (Router router : loadedRouters) {
            if (router.getIdNE() == id) {
                return router;
            }
        }
        return null;
    }

    /**
     * DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE |
     * DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE |
     * DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE |
     * @param id ID of a Router to seek
     * @return MAY RETURN AN INSTANCE WITHOUT AN OWNER
     */
    @Deprecated
    @Override
    public Router get (long id) {
        logger.error("DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | ");
        logger.error("DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | ");
        logger.error("DON'T USE | DON'T USE | DON'T USE | DON'T USE | DON'T USE | ");
        Router loadedRouter = getLoaded(id);
        if (null != loadedRouter) {
            return loadedRouter;
        }
        Optional<Router> newlyLoaded = routerRepository.findByIdNE(id);
        if (!newlyLoaded.isPresent()) {
            SequenceStatus.NOT_FOUND_IN_DATABASE.logWarning("Router", id);
            return null;
        }
        trackRecursively(newlyLoaded.get());
        return newlyLoaded.get();
    }

    @Override
    public SequenceStatus create (Router router) {
        if (null == router) {
            SequenceStatus.NULL_POINTER.logError("saveNew", "router");
            return SequenceStatus.NULL_POINTER;
        }
        long instanceId = router.getIdNE();
        if (instanceId != 0) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("0", "idNE", "router");
            throw new IllegalArgumentException();
        }

        Scene scene = SceneService.getInstance().getLoaded(router.getSceneId());
        if (null == scene) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            ArrayStoreException e = new ArrayStoreException();

            throw e;
        }
        router.setScene(scene);
        scene.addNetworkElement(router);

        routerConfigurer.givePortsTo(router);
        trackRecursively(router);
        update(router);

        logger.info("Following ports are given to router with id {}:", router.getIdNE());
        for (RouterPort port : router.getPorts()) {
            logger.info("Port with id {}", port.getId());
        }

        return SequenceStatus.OK;
    }

    @Override
    public Router update(Router router) {
        if (router.getScene() == null) {
            router.setScene(SceneService.getInstance().getLoaded(router.getSceneId()));
        }
        if (null == getLoaded(router.getIdNE())) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Router");
            loadedRouters.add(router);
        } else {
            getLoaded(router.getIdNE()).updateCurrentPosition(router);
        }
        Router savedRouter = routerRepository.save(router);

        if (savedRouter != router) {
            router.copyAutogeneratedValues(savedRouter);
        }
        return router;
    }

    @Override
    public void trackRecursively(Router router) {
        if (null == router) {
            SequenceStatus.NULL_POINTER.logError("loadRecursively", "instance");
            throw new NullPointerException();
        }
        if (loadedRouters.contains(router)) {
            return;
        }

        List<RouterPort> routerPorts = router.getPorts();
        for (RouterPort routerPort : routerPorts) {
            portService.trackRecursively(routerPort);
        }
        routingTableService.trackRecursively(router.getRoutingTable());
        loadedRouters.add(router);
    }

    @Override
    protected void untrackRecursively(Router instance) {
        if (null == instance) {
            SequenceStatus.NULL_POINTER.logError("untrackRecursively", "instance");
            throw new NullPointerException();
        }
        if (!loadedRouters.contains(instance)) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("RouterService");
            return;
        }
        List<RouterPort> routerPorts = instance.getPorts();
        for (RouterPort routerPort : routerPorts) {
            portService.untrackRecursively(routerPort);
        }
        routingTableService.untrackRecursively(instance.getRoutingTable());
        loadedRouters.remove(instance);
    }

    @Override
    protected void drop(Router instance) {
        if (null == instance) {
            SequenceStatus.NULL_POINTER.logError("dropFromDB", "instance");
            throw new NullPointerException();
        }
        routerRepository.delete(instance);
    }

    @Synchronized
    public SequenceStatus stop (Long routerId) {
        Router router = getLoaded(routerId);
        if (null == router) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        router.setWorking(false);
        List<Long> linkIds =
                router.getPorts().stream()
                        .filter(routerPort -> null != routerPort.getConnection())
                        .map(routerPort -> routerPort.getConnection().getLink().getId())
                        .collect(Collectors.toList());
        Scene scene = router.getScene();

        scene.getNetworkElements().remove(router);
        untrackRecursively(router);

        Optional<Router> routerOpt = routerRepository.findByIdNE(routerId);
        if (routerOpt.isPresent())
            router = routerOpt.get();
        scene.getNetworkElements().add(router);
        trackRecursively(router);

        for (Long linkId : linkIds) {
            linkService.linkComps(linkId);
        }

        return SequenceStatus.OK;
    }

    @Override
    public SequenceStatus delete(Router instance) {
        throw new NotImplementedException();
    }


    /**
     * Recursively deletes LOADED router with the given id.
     * Links connected to the router's ports are deleted as well.
     * If router is not loaded, does nothing.
     * @return
     */
    public SequenceStatus delete (Long idNE) {
        if (null == idNE) {
            SequenceStatus.NULL_POINTER.logError("delete", "idNE");
            return SequenceStatus.NULL_POINTER;
        }

        Router router = getLoaded(idNE);
        if (null == router) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        Scene scene = router.getScene();
        Collection<Link> sceneLinks = scene.getLinks();
        Collection<RouterPort> ports = router.getPorts();

        for (RouterPort port : ports) {
            Link.Connection connection = port.getConnection();
            if (null != connection) {
                Link link = port.getConnection().getLink();
                sceneLinks.remove(link);
                linkService.delete(link);
            }
        }
        scene.getNetworkElements().remove(router);
        untrackRecursively(router);
        drop(router);
        return SequenceStatus.OK;
    }

    public ResponseEntity setIpAddress(SetIpDTO setIpDTO) {
        return setIpAddress(setIpDTO.getRouterId(), setIpDTO.getPortNumber(),
                            setIpDTO.getIp(), setIpDTO.getMask());
    }

    public ResponseEntity setIpAddress(Long routerId, Integer portNumber, String ip, String mask) {
        Router router = getLoaded(routerId);
        if (null == router) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body(String.format("Router with id %d couldn't have been found.", routerId));
        }
        RouterPort port = router.getPort(portNumber);
        if (null == port) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body(String.format("Port with number %d couldn't have been found.", portNumber));
        }

        port.setAddress(ip, mask);
        routingTableService.routeDirectConnection(port);
        Gson gson = new Gson();
        logger.info("RouterID {}, port {} (portId {}), ip {}.", routerId, portNumber, port.getId(), IpService.stringFromInt(router.getPort(portNumber).getIp()));
        return ResponseEntity.ok().body(gson.toJson("ok"));
    }

    public RoutingTable routeStatic (StaticRouteDTO staticRouteDTO) {
        Router router = getLoaded(staticRouteDTO.getRouterId());
        if (null == router) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Router");
            throw new RuntimeException();
        }
        routingTableService.routeStatic(router.getRoutingTable(),
                                        IpService.intFromString(staticRouteDTO.getIp()),
                                        IpService.intFromString(staticRouteDTO.getMask()),
                                        IpService.intFromString(staticRouteDTO.getNextHop()));

        return router.getRoutingTable();
    }
}
