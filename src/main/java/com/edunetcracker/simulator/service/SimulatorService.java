package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.database.repository.LinkRepository;
import com.edunetcracker.simulator.model.DTO.PingDTO;
import com.edunetcracker.simulator.model.Scene;
import com.edunetcracker.simulator.model.context.generatedConfig.GeneratedConfig;
import com.edunetcracker.simulator.model.context.NEContext;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.service.context.ContextService;
import com.edunetcracker.simulator.service.routingService.IpService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//import static com.edunetcracker.simulator.model.context.TrafficContext.TrafficType.SIMPLE;


@Service
public class SimulatorService {
    private static Logger logger = LoggerFactory.getLogger(SimulatorService.class);

    private final LinkRepository linkRepository;
    private final RouterService routerService;
    private final SwitchService switchService;
    private final ContextService contextService;
    private final SceneService sceneService;

    @Autowired
    public SimulatorService(LinkRepository linkRepository, RouterService routerService, SwitchService switchService, ContextService contextService, SceneService sceneService) {
        this.linkRepository = linkRepository;
        this.routerService = routerService;
        this.switchService = switchService;
        this.contextService = contextService;
        this.sceneService = sceneService;
    }


    public ResponseEntity<List<NetworkElement>> getComps () {
//        чтение из бд сохраненных параметров
// fixme: wrong method, it is necessary to return object with all information about Scene

        List<NetworkElement> comps = new ArrayList<>();

        return ResponseEntity.status(HttpStatus.OK).body(comps);
    }


    public ResponseEntity startComponent(Long id) {
        if (null == id) {
            SequenceStatus.NULL_POINTER.logError("startComponent", "id");
            return null;
        }
        Gson gson = new Gson();
        NetworkElement ne = switchService.getLoaded(id);
        if (null != ne) {
            ne.start();
            return ResponseEntity.ok().body(gson.toJson("Started switch successfully."));
        }
        ne = routerService.getLoaded(id);
        if (null != ne) {
            ne.start();
            return ResponseEntity.ok().body(gson.toJson( "Started router successfully."));
        }
        logger.error("Tried to start nonexisting NetworkElement with id {}", id);
        return ResponseEntity.badRequest()
                .body(gson.toJson(String.format("Tried to start nonexisting NetworkElement with id %d", id)));
    }

    public ResponseEntity startAllComponents(Long sceneId) {
        if (null == sceneId) {
            SequenceStatus.NULL_POINTER.logError("startAllComponents", "sceneId");
            return null;
        }
        Scene scene = sceneService.getLoaded(sceneId);
        if (null == scene) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            return null;
        }
        List<NetworkElement> networkElements = scene.getNetworkElements();

        for (NetworkElement ne : networkElements) {
            ne.start();
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson("All components have been started"));
    }


    public ResponseEntity startComponents (List<Integer> ids) {
        for (Integer id : ids) {
            NetworkElement ne = switchService.getLoaded(id);
            if (null == ne) {
                ne = routerService.getLoaded(id);
            }
            if (null != ne) {
                ne.setWorking(true);
                ne.start();
            } else {
                logger.error("Tried to start nonexisting NetworkElement with id {}", id);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body("Request components have been started");
    }

    public ResponseEntity stopComponent (Long id) {
        if (null == id) {
            SequenceStatus.NULL_POINTER.logError("stopComponent", "id");
            return null;
        }
        Gson gson = new Gson();
        NetworkElement ne = switchService.getLoaded(id);
        if (null != ne) {
            switchService.stop(ne.getIdNE());
            return ResponseEntity.ok().body(gson.toJson("Stopped switch successfully."));
        }
        ne = routerService.getLoaded(id);
        if (null != ne) {
            routerService.stop(ne.getIdNE());
            return ResponseEntity.ok().body(gson.toJson( "Stopped router successfully."));
        }
        logger.error("Tried to stop nonexisting NetworkElement with id {}", id);
        return ResponseEntity.badRequest()
                .body(gson.toJson(String.format("Tried to stop nonexisting NetworkElement with id %d", id)));
    }


    public ResponseEntity stopAllComponents (Long sceneId) {
        if (null == sceneId) {
            SequenceStatus.NULL_POINTER.logError("stopAllComponents", "sceneId");
            return null;
        }
        Scene scene = sceneService.getLoaded(sceneId);
        if (null == scene) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            return null;
        }
        List<NetworkElement> networkElements = scene.getNetworkElements();

        for (NetworkElement ne : networkElements) {
            ne.setWorking(false);
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson("All components have been stopped"));
    }

    public ResponseEntity stopComponents (List<Integer> ids) {

        return ResponseEntity.status(HttpStatus.OK).body("Request components have been stopped");
    }

    /**
     * Creates a ping context for a router.
     * (All the work of finding necessary port is on context's shoulders).
     * @param pingDTO
     * @return
     */
    public NEContext ping (PingDTO pingDTO) {
        Router router = routerService.getLoaded(pingDTO.getRouterId());
        logger.trace("Requested router id: {}. Found: {}", pingDTO.getRouterId(), router);
        NEContext ping = contextService.ping(router.getSessionId(), null, IpService.intFromString(pingDTO.getDestIp()));
        router.getContexts().add(ping);

        return ping;
    }

    /**
     * An Entity to test traffic function
     * GeneratedConfig is a DAO, where we write our params (so that not to
     * send them separately each time, but rather collect in one class)
    // * @param generatedConfig (class of parameters for traffic)
     * @return ok statement if everything works fine
     */
    public ResponseEntity traffic (GeneratedConfig generatedConfig){
        Router router = routerService.getLoaded(generatedConfig.getRouterId());

        NEContext traffic = contextService.traffic(generatedConfig);

        router.getContexts().add(traffic);

        return ResponseEntity.ok(traffic.getId());
    }

}
