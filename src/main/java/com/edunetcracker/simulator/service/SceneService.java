package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.database.repository.SceneDTORepository;
import com.edunetcracker.simulator.database.repository.SceneRepository;
import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.Scene;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.element.switchNE.Switch;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SceneService extends DBService<Scene> {
    private static Logger logger = LoggerFactory.getLogger(SceneService.class);
    //Todo: Need to atomize the operations
    @Getter
    private static SceneService instance;


    private final SceneDTORepository sceneDTORepository;
    private final SceneRepository sceneRepository;

    private final RouterService routerService;
    private final SwitchService switchService;
    private final LinkService linkService;


    private static final String SCENE_DEFAULT_NAME = "Sample Text";

    @Getter
    private List<Scene> loadedScenes = new ArrayList<>();

    @Autowired
    public SceneService(SceneDTORepository sceneDTORepository, SceneRepository sceneRepository, RouterService routerService, SwitchService switchService, LinkService linkService) {
        this.sceneDTORepository = sceneDTORepository;
        if (null != instance) {
            logger.error("HOW CAN IT BE? Detected a trial to initialize another instance of the service!");
        }
        instance = this;

        this.sceneRepository = sceneRepository;
        this.routerService = routerService;
        this.switchService = switchService;
        this.linkService = linkService;
    }

    public SequenceStatus delete(int id) {
        Scene scene = getLoaded(id);
        if (null == scene) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        untrackRecursively(scene);
        Collection<Link> links = scene.getLinks();
        Collection<NetworkElement> networkElements = scene.getNetworkElements();
        for (Link link : links) {
            linkService.delete(link.getId());
        }
        for (NetworkElement networkElement : networkElements) {
            if (networkElement instanceof Switch) {
                switchService.delete(networkElement.getIdNE());
            }
            if (networkElement instanceof Router) {
                routerService.delete(networkElement.getIdNE());
            }
        }
        drop(scene);
        return SequenceStatus.OK;
    }

    public Scene change(int id, Scene scene){
        loadedScenes.get(id).setName(scene.getName());
        loadedScenes.get(id).setLinks(scene.getLinks());
        loadedScenes.get(id).setNetworkElements(scene.getNetworkElements());
        return loadedScenes.get(id);
    }

    @Override
    public Scene getLoaded(long id) {
        for (Scene scene : loadedScenes) {
            if (scene.getId() == id) {
                return scene;
            }
        }
        return null;
    }

    private Scene getLoadedByName(String name) {
        if (null == name) {
            SequenceStatus.NULL_POINTER.logError("getLoadedByName", "name");
            throw new NullPointerException();
        }
        for (Scene scene : loadedScenes) {
            if (name.equals(scene.getName())) {
                return scene;
            }
        }
        return null;
    }

    @Override
    public void trackRecursively(Scene scene) {
        if (null == scene) {
            throw new NullPointerException();
        }
        if (loadedScenes.contains(scene)) {
            return;
        }

        if (null != scene.getNetworkElements()) {
            for (NetworkElement ne : scene.getNetworkElements()) {
                if (ne instanceof Router) routerService.trackRecursively((Router)ne);
                if (ne instanceof Switch) switchService.trackRecursively((Switch)ne);
            }
        }
        if (null != scene.getLinks()) {
            for (Link link : scene.getLinks()) {
                linkService.linkComps(link);
            }
        }

        loadedScenes.add(scene);
    }

    public Map<Long, String> getSceneNames () {
        List<SceneDTO> sceneList = sceneDTORepository.findAll();
        Map<Long, String> namesMap =
                sceneList.stream()
                .collect(Collectors.toMap(SceneDTO::getId, SceneDTO::getName));
        return namesMap;
    }

    @Override
    public Scene get(long id) {
        Scene loadedScene = getLoaded(id);
        if (null != loadedScene) {
            return loadedScene;
        }
        Optional<Scene> newlyLoaded = sceneRepository.findById(id);
        if (!newlyLoaded.isPresent()) {
            SequenceStatus.NOT_FOUND_IN_DATABASE.logWarning("Scene", id);
            return null;
        }
        trackRecursively(newlyLoaded.get());

        return newlyLoaded.get();
    }

    public Scene getByName (String name) {
        if (null == name) {
            SequenceStatus.NULL_POINTER.logError("getByName", "name");
            throw new NullPointerException();
        }
        Scene loadedScene = getLoadedByName(name);
        if (null != loadedScene) {
            return loadedScene;
        }
        Optional<Scene> newlyLoaded = sceneRepository.findByName(name);
        return newlyLoaded.orElse(null);
    }

    @Override
    public SequenceStatus create(Scene scene) {
        if (null == scene) {
            SequenceStatus.NULL_POINTER.logError("saveNew", "scene");
            return SequenceStatus.NULL_POINTER;
        }
        long instanceId = scene.getId();
        if (instanceId != 0) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("0", "ID", "Scene DTO");
        }

        if (null == scene.getName()) {
            scene.setName(SCENE_DEFAULT_NAME);
        }
        trackRecursively(scene);
        logger.info("Scene id: {}", scene.getId());
        update(scene);
        return SequenceStatus.OK;
    }

    /**
     * Updates loaded scene in a database.
     * If a scene with the id wasn't loaded, nothing happens.
     * @param id Id of a scene to change.
     */
    public Scene update (Long id) {
        if (null == id) {
            throw new NullPointerException("Received null pointer as id.");
        }
        logger.info("Updating scene with ID{}", id);
        Scene scene = getLoaded(id);
        if (null == scene) {
            return null;
        }
        return update(scene);
    }

    @Override
    public Scene update(Scene scene) {
        if (null == scene) {
            SequenceStatus.NULL_POINTER.logError("updateInDB", "scene");
            throw new NullPointerException();
        }
        if (null == getLoaded(scene.getId())) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            trackRecursively(scene);
        }
        logger.info("Scene id: {}", scene.getId());
        Scene savedScene = sceneRepository.save(scene);

        if (savedScene != scene) {
            scene.copyAutogeneratedValues(savedScene);
        }
        return scene;
    }

    @Override
    public void untrackRecursively(Scene instance) {
        if (null == instance) {
            SequenceStatus.NULL_POINTER.logError("untrackRecursively", "instance");
            throw new NullPointerException();
        }
        if (!loadedScenes.contains(instance)) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("SceneService");
            return;
        }

        if (null != instance.getNetworkElements()) {
            for (NetworkElement ne : instance.getNetworkElements()) {
                if (ne instanceof Router) routerService.untrackRecursively((Router)ne);
                if (ne instanceof Switch) switchService.untrackRecursively((Switch)ne);
            }
        }
        if (null != instance.getLinks()) {
            for (Link link : instance.getLinks()) {
                linkService.untrackRecursively(link);
            }
        }

        loadedScenes.remove(instance);
    }

    @Override
    public void drop(Scene instance) {
        if (null == instance) {
            SequenceStatus.NULL_POINTER.logError("dropFromDB", "instance");
            throw new NullPointerException();
        }
        sceneRepository.delete(instance);
    }

    @Override
    public SequenceStatus delete(Scene instance) {
        throw new NotImplementedException();
    }
}
