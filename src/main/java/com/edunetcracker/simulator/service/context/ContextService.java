package com.edunetcracker.simulator.service.context;

import com.edunetcracker.simulator.model.context.NEContext;
import com.edunetcracker.simulator.model.context.PingContext;
import com.edunetcracker.simulator.model.context.TrafficContext;
import com.edunetcracker.simulator.model.context.generatedConfig.GeneratedConfig;
import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.ip.IP;
import com.edunetcracker.simulator.service.WSService;
import com.edunetcracker.simulator.service.routingService.IpService;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContextService {

    @Getter
    private static ContextService instance;

    private final WSService wsService;

    private static Logger logger = LoggerFactory.getLogger(ContextService.class);

    private long contextCounter = 1;

    @Autowired
    public ContextService(WSService wsService) {
        if (null == instance) {
            instance = this;
        }
        this.wsService = wsService;
    }


    /**
     * Makes context out of incoming ipPacket, destination of which
     * is no the router calling this method.
     * @param ipPacket
     * @return
     */
    public NEContext fromTransitionalIp(IP ipPacket) {
        //TODO(Wisp): implement fromTransitionalIP (IP ipPacket)
        logger.warn("ContextService.fromTransitionalIp(...) is not implemented!!");
        return null;
    }

    public NEContext fromAcceptedIp (Long sessionId, IP ipPacket) {
        if (null == ipPacket) {
            throw new NullPointerException("Handler of accepted IP packets received null");
        }

        if (DataUnit.Type.PING == ipPacket.getEncapsulatedType()) {
            logger.info("Making ping context from accepted Ping. Source of ping: {}", IpService.stringFromInt(ipPacket.getSourceIp()));
            PingContext context = new PingContext();
            context.setResponsive(true);
            context.setWsService(wsService);
            context.setSessionId(sessionId);
            context.setId(Long.parseLong(ipPacket.getExtraData()));
            context.setSourceIp(ipPacket.getDestinationIp());
            context.setDestinationIp(ipPacket.getSourceIp());
            context.setPingsLeft(0);
            context.setAlive(true);
            return context;
        }

        return null;
    }

    public PingContext ping (Long sessionId, Integer sourceIp, Integer destinationIp) {
        PingContext context = new PingContext();
        context.setResponsive(false);
        context.setWsService(wsService);
        context.setSessionId(sessionId);
        context.setGotResponse(false);
        context.setId(contextCounter++);
        context.setSourceIp(sourceIp);
        context.setDestinationIp(destinationIp);
        context.setPingsLeft(6);
        context.setAlive(true);
        return context;
    }

    //todo(dasha):create similar func to traffic
    public NEContext traffic(GeneratedConfig config){
        return new TrafficContext(config);
    }

}
