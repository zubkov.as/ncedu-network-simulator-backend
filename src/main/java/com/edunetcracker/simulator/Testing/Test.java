package com.edunetcracker.simulator.Testing;

import com.edunetcracker.simulator.service.RouterService;
import com.edunetcracker.simulator.service.SceneService;
import com.edunetcracker.simulator.service.SimulatorService;
import com.edunetcracker.simulator.service.configurers.RouterConfigurer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
/**
 * Test which creates a simulation
 */
public class Test {
    private static SimulatorService simulatorService;

    @Autowired
    public Test(SceneService sceneService, RouterService routerService, RouterConfigurer routerConfigurer, SimulatorService simulatorService) {
        this.simulatorService = simulatorService;
    }
/*
    public static void doTest(TrafficDTO trafficDTO) {
        simulatorService.traffic(trafficDTO);
    }*/
}