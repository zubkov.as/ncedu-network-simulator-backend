package com.edunetcracker.simulator.database.repository;

import com.edunetcracker.simulator.model.DTO.SceneDTO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SceneDTORepository extends CrudRepository<SceneDTO, Long> {

    List<SceneDTO> findAll ();
}
